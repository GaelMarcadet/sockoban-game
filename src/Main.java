import controller.Controller;
import controller.ControllerFX;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import loader.Game;
import loader.Loader;
import view.ButtonView;
import view.PanView;
import view.builder.Builder;
import view.LabelView;

import java.util.Locale;

/**
 * The class Main {@code Main} is the main class of te project, it extends the JavaFX Application class and it launch a GUI
 * The project is divided in three part implementing the Model - View - Controller architecture
 */
public class Main extends Application {

    /**
     * Method that launch the JavaFX application
     */
    public static void main(String[] args) {
        Locale.setDefault(Locale.FRANCE);
        launch(new String[]{});

    }

    @Override
    /**
     * Instanciation of the MVC architecture and classes
     * */
    public void start(Stage primaryStage) throws Exception {

        //The controller of the projet, implementing the singleton pattern
        Controller controller = Controller.getController();

        // a part of the view
        PanView panView = new PanView();


        // the other part of the view where all the buttons and labels are stored
        ButtonView buttonView = new ButtonView();
        LabelView labelView = new LabelView();
        //Adding the panView and the buttonView as Observers of the controller, so that any change of the model trigger
        // the update methods of buttonView and panView via the controller methods notify
        controller.addObserver(panView);
        controller.addObserver(buttonView);
        controller.addObserver(labelView);

        // Class that implement the builder pattern, that create a complex object with every node of the view
        Builder builder = new Builder();
        // adding the buttons at the bottom of the GUI
        builder.addBottom(buttonView.getReset(), buttonView.getUndo(), buttonView.getRedo(), buttonView.getAnimated());

        // adding the label to the GUI
        builder.setMoveCounterLabel(labelView.getNbrAction());

        // adding the label to the GUI
        builder.setPushCounterLabel(labelView.getNbrPush());


        //charging the list of the games
        ObservableList<Game> games = FXCollections.observableArrayList(Loader.loadGames());
        ListView<Game> gameListView = new ListView<>(games);

        Game initialGame = null;
        for (Game game : games) {
            if (game.getTitle().equals("Tutorial")) {
                initialGame = game;
                break;
            }
        }
        if (initialGame == null)
            initialGame = games.get(0);


        Scene scene = builder
                .setCenter(panView.getGridPane(), panView::update)
                .setExposedGames(gameListView, controller::setGame)
                .setMoveCounterLabel(labelView.getNbrAction())
                .setPushCounterLabel(labelView.getNbrPush())
                .generateScene();

        //Class that bind every keystroke and buttons to an action
        ControllerFX controllerFX = new ControllerFX(controller, panView, buttonView, scene);

        controller.setControllerFX(controllerFX);
        controller.setGame(initialGame);



        primaryStage.setScene(scene);
        primaryStage.setTitle("Sokoban le Bricoleur");
        primaryStage.show();
    }

}