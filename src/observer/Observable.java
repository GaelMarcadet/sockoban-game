package observer;


/**
 * The interface Observable {@code Observable} is used to implement the observer - observable pattern.
 * This interface allow to store multiple observer and to notify them all with the use of the notifie() method
 *
 * @see Observer
 */
public interface Observable {


    /**
     * Add an observer to the collection of observers of the class
     */
    public void addObserver(observer.Observer observer);

    /**
     * Notify all the observers of the class, triggering the method update() of each observers
     *
     * @see Observer
     */
    public void notifie();

}
