package loader;

public class Character extends Coordinates {
    public Character(int x, int y) {
        super(x, y);
    }

    /**
     * Changes the position (x,y) of this
     *
     * @param x int the position on x
     * @param y int the position on y
     */
    public void set(int x, int y) {
        this.setX(x);
        this.setY(y);
    }
}
