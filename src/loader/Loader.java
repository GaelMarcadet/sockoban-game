package loader;


import java.io.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.regex.*;
import java.util.stream.Stream;

import static loader.Type.*;

/**
 * The {@code Loader} class is used to load Sockoban game plate from external files with .txt or .xsb extensions.
 * This kind of plates is represented with specifics characters which are specified in enum {@code Type}.
 * <p>
 * The class loads each file which matched with a valid extension.
 * Then each plate in this file will be loaded independently from other.
 * Thus, if an error occurs during loading process of a game, only her will failed.
 * <p>
 * The only one static function callable from the exterior is {@code loadGames}.
 * This function is used to load all files (by inference, all games) contained in xsbfiles project directory.
 *
 * @author gael
 */
public class Loader {

    /**
     * Loads all games contained in MicroCosmos.txt
     *
     * @return All games interpreted by the loader.
     */
    public static ArrayList<Game> loadGames() {

        ArrayList<Game> res = new ArrayList<>();

        BufferedReader reader = new BufferedReader(
                new InputStreamReader(Loader.class.getResourceAsStream("/xsbfiles/MicroCosmos.txt")));

        res.addAll(loadXSBStream("MicroCosmos.txt", reader.lines()));

        return res;
    }

    /**
     * This function loads all available games in the file {@code xsbfile}.
     *
     * @param stream The stream which contains all lines contained in one file.
     * @return All interpreted games.
     * @throws IOException If {@code xsbfile} isn't well formatted.
     */
    private static LinkedList<Game> loadXSBStream(String filename, Stream<String> stream) {

        LinkedList<Game> games = new LinkedList<>();
        Pattern title_line_pattern = Pattern.compile("^;");


        // Max_x is the max number of char detected in line
        // Max_y is the number of line in the file (Commentaries excluded)
        int max_x = 0, max_y = 0;

        LinkedList<String> lines = new LinkedList<>();

        LinkedList<String> fileLines = new LinkedList<>();
        stream.forEach(fileLines::add);

        for (String line : fileLines) {
            if (!line.isEmpty()) {
                if (title_line_pattern.matcher(line).find()) {
                    line = line.trim();
                    try {
                        Game game = generateGame(lines, line.split(";")[1], filename , max_x, max_y);
                        games.addLast(game);

                    } catch (IOException e) {
                        System.out.println("[LOG]" + e);
                    }
                    lines.clear();
                    max_x = max_y = 0;
                } else {
                    lines.addLast(line);
                    int line_length = line.length();
                    if (max_x < line_length)
                        max_x = line_length;
                    ++max_y;
                }
            }
        }
        return games;
    }

    /**
     * This function transforms all strings into a game object.
     *
     * @param lines Lines of the game.
     * @param title Title of the game.
     * @param filename  File which contains the game.
     * @param max_x Number of columns.
     * @param max_y Number of lines.
     * @return Game object created from the file.
     * @throws IOException If the file isn't well formatted.
     */
    private static Game generateGame(LinkedList<String> lines, String title, String filename, int max_x, int max_y) throws IOException {
        Character character = null;
        Type[][] map = new Type[max_x][max_y];
        int boxs = 0, boxs_on_goal = 0;
        int x = 0, y = 0;
        for (String s : lines) {
            x = 0;
            int s_length = s.length();
            for (int index = 0; index < max_x; ++index) {
                if (index < s_length) {
                    char c = s.charAt(index);

                    if (PLAYER_ON_FLOOR.equals(c)) {
                        if (character != null)
                            throw new GameOpeningFailedException(filename, title, "This is too more characters");
                        character = new Character(x, y);
                        map[x][y] = PLAYER_ON_FLOOR;

                    } else if (PLAYER_ON_GOAL.equals(c)) {
                        if (character != null)
                            throw new GameOpeningFailedException(filename, title, "This is too more characters");
                        character = new Character(x, y);
                        map[x][y] = PLAYER_ON_GOAL;

                    } else if (BOX_ON_FLOOR.equals(c)) {
                        map[x][y] = BOX_ON_FLOOR;
                        ++boxs;

                    } else if (BOX_ON_GOAL.equals(c)) {
                        map[x][y] = BOX_ON_GOAL;
                        ++boxs;
                        ++boxs_on_goal;


                    } else if (EMPTY_GOAL.equals(c)) {
                        map[x][y] = EMPTY_GOAL;

                    } else if (WALL.equals(c)) {
                        map[x][y] = WALL;

                    } else if (EMPTY.equals(c)) { // Empty value
                        map[x][y] = EMPTY;
                    } else {
                        throw new GameOpeningFailedException(filename, title, "Invalid character detected at line " + (++y) + ": " + c);
                    }


                } else {
                    map[x][y] = EMPTY;
                }
                x++;
            }
            y++;

        }
        if (character == null) {
            throw new GameOpeningFailedException(filename, title,": No character detected");
        }

        return new Game(map, character, boxs, boxs_on_goal, title, filename);
    }


    /**
     * This exception occurs when the game cannot be opened by the load functions.
     */
    static private class GameOpeningFailedException extends IOException {
        private String gameName, fileName;

        public GameOpeningFailedException(String fileName, String gameName, String message) {
            super(message);
            this.gameName = gameName;
            this.fileName = fileName;
        }

        public String toString() {
            return "Opening failed: " + fileName + ": " + gameName + ": " + getMessage();
        }
    }
}

