package exceptions;

/**
 * The class NoPreviousException {@code NoPreviousException} is an exception that is used to warn the user about the lack
 * of previous moves at the beginning of the round if the user want to use the method.
 *
 * @see commands.LastDirectionCommand
 */
public class NoPreviousDirectionsException extends Exception {

    public NoPreviousDirectionsException(String message) {

        super(message);
    }
}
