package Model;

import loader.Direction;

/**
 * MVC Structure Model Interface
 */
public interface Model {

    /**
     * Move the character if he can and the block ahead (if there is one)
     *
     * @param d the direction where the player wants to go
     * @return  1 if the move has succeed with a block
     *          0 if the move has succeed without a block
     *          -1 if the move aren't possible
     */
    int move(Direction d);
}
