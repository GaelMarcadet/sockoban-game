package Model;

import loader.Coordinates;
import loader.Direction;
import loader.Game;
import loader.Type;
import java.util.ArrayList;

/** Model's implementation with an ArrayList
 * for store each move
 */
public class ModelMemory implements Model {

    /** An instance of ModelConcret for
     * instantiating a game
     */
    private ModelConcret model_concret;

    /** An ArrayList which store each move
     */
    private ArrayList<memorySoko> memory;

    /** an index which point the current state of
     * the game in the memory
     */
    private int indexCurrent;



    /**Constructor without parameter
     */
    ModelMemory(){
        this.model_concret = new ModelConcret();
        this.memory = new ArrayList<>();
        this.indexCurrent = -1;
    }

    /**Constructor with one parameter
     *
     * @param game  The game asked by the user
     */
     ModelMemory(Game game){
        this.model_concret = new ModelConcret(game);
        this.memory = new ArrayList<>();
        this.indexCurrent = -1;
    }




    /** Changes the current game
     *
     * @param game the new game asked by the user
     */
    public void setGame(Game game) {
        this.model_concret.setGame(game);
    }

    /** Gives the current game
     *
     * @return game the current game
     */
    public Game getGame() {
        return model_concret.getGame();
    }

    /** Gives the memory
     *
     * @return ArrayList<memorySoko> the memory
     */
    ArrayList<memorySoko> getMemory() {
        return memory;
    }

    /**
     * Gives the current index
     *
     * @return int the current index
     */
    int getIndexCurrent(){ return this.indexCurrent; }


    /** Moves the character if he can and the block ahead (if there is one)
     *  and stores the movement made in the memory
     *
     * @param d     the direction where the player wants to go
     *
     * @return 1 if the move has succeed with a block
     *         0 if the move has succeed without a block
     *         -1 if the move aren't possible
     */
    @Override
    public int move(Direction d) {
        Game g = model_concret.getGame();
        Coordinates character = new Coordinates(g.getCharacter().getX(),g.getCharacter().getY());

        int res = model_concret.move(d);

        if( res > -1){

            if(indexCurrent != -1) memory.get(indexCurrent).setCurrentState(false);

            if(res == 0){ inserer(new memorySoko(d,character)); }

            else{
                Coordinates box = new Coordinates(g.getCharacter().getX(),g.getCharacter().getY());
                inserer(new memoryBox(d,character,box));
            }

        }
        //System.out.println(indexCurrent);
        return res;
    }

    /** Undone the move made if there is one to undo
     *
     * @return 1 if the undo has succeed with a block
     *         0 if the undo has succeed without a block
     *         -1 if the undo aren't possible
     */
    int undo(){
        int res = -1;
        if(!memory.isEmpty()){
            memorySoko m = getCurrentState();
            if(m!=null) {

                Type[][] p = model_concret.getGame().getMap();
                Coordinates pos = new Coordinates(m.getCharacter().getX() + m.getD().getOffset_x(), m.getCharacter().getY() + m.getD().getOffset_y());
                Type type = p[pos.getX()][pos.getY()];
                Type type_nextPos = p[m.getCharacter().getX()][m.getCharacter().getY()];
                model_concret.switchPlace(type, type_nextPos, m.getCharacter(), pos);
                res = 0;
                if (m instanceof memoryBox) {

                    type_nextPos = p[((memoryBox) m).getBox().getX()][((memoryBox) m).getBox().getY()];
                    pos.setX(((memoryBox) m).getBox().getX() + m.getD().getOffset_x());
                    pos.setY(((memoryBox) m).getBox().getY() + m.getD().getOffset_y());
                    type = p[pos.getX()][pos.getY()];
                    model_concret.switchPlace(type, type_nextPos, ((memoryBox) m).getBox(), pos);
                    res = 1;
                }
                m.setCurrentState(false);
                back();
                indexCurrent--;
                return res;
            }

        }
        //System.out.println(indexCurrent);
        return res;
    }

    /** redone the move undone earlier
     *
     * @return 1 if the redo has succeed with a block
     *         0 if the redo has succeed without a block
     *         -1 if the redo aren't possible
     */
    int redo(){
        if(indexCurrent < memory.size()-1){
            int res = move(memory.get(indexCurrent+1).getD());
            return res;
        }
        return -1;

    }

    /** Resets the game at the initial state
     */
    void reset(){
        while(indexCurrent > -1){ undo(); }
        memory.clear();
    }



    /** Inserts the move in the memory, if the current index points
     *  a place which is already used the new move crush the old move,
     *  else it's add at the end of the ArrayList
     *
     * @param m The new movement to be stored
     */
    private void inserer(memorySoko m){
        if(indexCurrent == memory.size()-1){
            memory.add(m);
        }else{
            memory.set(indexCurrent+1,m);
        }
        indexCurrent++;
    }

    /** Changes the current state with the previous state
     */
    private void back(){
        if(indexCurrent>0) {
            memory.get(indexCurrent - 1).setCurrentState(true);
        }
    }

    /** Gives the current state
     *
     * @return memorySoko the current state if she exist
     *         else null
     */
    private memorySoko getCurrentState(){
        if(indexCurrent>=0) {
            return memory.get(indexCurrent);
        }else{
            return null;
        }
    }


    /** Temporary method
     */
    void printGame(){ model_concret.printGame();
        System.out.println(this.memory.toString());
        System.out.println("Index courant : " + indexCurrent); }
}
