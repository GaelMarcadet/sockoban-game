package Model;

import loader.Direction;
import loader.Game;

import java.util.ArrayList;

/**
 * Model's implementation with move's number
 * and move's number of undone
 */
public class ModelMoveNumber implements Model {

    /**
     * An instance of ModelMemory for
     * instantiating a game
     */
    private ModelMemory model_memory;

    /**
     * move's number
     */
    private int move_number;

    /**
     * move's number of undone
     */
    private int move_number_undo;

    /**
     * Constructor without parameter
     */
    public ModelMoveNumber() {
        this.model_memory = new ModelMemory();
        this.move_number = 0;
        this.move_number_undo = 0;
    }

    /**
     * Constructor with one parameter
     *
     * @param game The game asked by the user
     */
    public ModelMoveNumber(Game game) {
        this.model_memory = new ModelMemory(game);
        this.move_number = 0;
    }

    /**
     * Changes the current game
     *
     * @param game the new game asked by the user
     */
    public void setGame(Game game) {
        this.model_memory.setGame(game);
    }

    /**
     * Gives the current game
     *
     * @return game the current game
     */
    public Game getGame() {
        return model_memory.getGame();
    }

    /**
     * Gives move's number
     *
     * @return int the move's number
     */
    public int getMove_number() {
        return move_number;
    }

    /**
     * Gives move's number of undone
     *
     * @return int the move's number of undone
     */
    public int getMove_number_undo() {
        return move_number_undo;
    }

    /**
     * Gives the memory
     *
     * @return ArrayList the memory
     */
    public ArrayList<memorySoko> getMemory() {
        return model_memory.getMemory();
    }

    /**
     * Gives the current index
     *
     * @return int the current index
     */
    public int getIndexCurrent(){ return  model_memory.getIndexCurrent(); }


    /**
     * Moves the character if he can and the block ahead (if there is one)
     * and increments the number of movements
     *
     * @param d the direction where the player wants to go
     * @return 1 if the move has succeed with a block
     * 0 if the move has succeed without a block
     * -1 if the move aren't possible
     */
    public int move(Direction d) {
        int res = model_memory.move(d);
        if (res > -1) {
            move_number++;
        }
        return res;
    }

    /**
     * Undo the move made if there is one to undo,
     * increments the number of movements of undo
     * and decrements the number of movements
     */
    public int undo() {
        int res = model_memory.undo();
        if (res > -1) {
            move_number_undo++;
            move_number--;
        }
        return res;
    }

    /**
     * redone the move undone earlier,
     * increments the number of movements
     * and decrements the number of movements of undo
     */
    public int redo() {
        int res = model_memory.redo();
        if (res > -1) {
            move_number_undo--;
            move_number++;
        }
        return res;
    }

    /**
     * Resets the game at the initial state
     */
    public void reset() {
        model_memory.reset();
        move_number = 0;
        move_number_undo = 0;
    }


    /**
     * Temporary method
     */
    public void printGame() {
        model_memory.printGame();
        System.out.println("Nombre de mouvement : " + this.move_number);
    }
}
