package Model;

import loader.Direction;
import loader.Game;
import java.util.ArrayList;

public class ModelPushNumber implements Model {

    /**
     * An instance of ModelMoveNumber for
     * instantiating a game
     */
    private ModelMoveNumber model_move_number;

    /**
     * push's number of undone
     */
    private int push_number;


    /**
     * Constructor without parameter
     */
    public ModelPushNumber() {
        this.model_move_number = new ModelMoveNumber();
        this.push_number = 0;
    }

    /**
     * Constructor with one parameter
     *
     * @param game The game asked by the user
     */
    public ModelPushNumber(Game game) {
        this.model_move_number = new ModelMoveNumber(game);
        this.push_number = 0;
    }


    /**
     * Changes the current game
     *
     * @param game the new game asked by the user
     */
    public void setGame(Game game) {
        reset();
        this.model_move_number.setGame(game);
    }

    /**
     * Gives the current game
     *
     * @return game the current game
     */
    public Game getGame() {
        return model_move_number.getGame();
    }

    /**
     * Gives push's number
     *
     * @return int the push's number
     */
    public int getPush_number() {
        return push_number;
    }

    /**
     * Gives move's number
     *
     * @return int the move's number
     */
    public int getMove_number() {
        return model_move_number.getMove_number();
    }

    /**
     * Gives move's number of undone
     *
     * @return int the move's number of undone
     */
    public int getMove_number_undo() {
        return model_move_number.getMove_number_undo();
    }


    /**
     * Gives the memory
     *
     * @return ArrayList the memory
     */
    public ArrayList<memorySoko> getMemory() {
        return model_move_number.getMemory();
    }

    /**
     * Gives the current index
     *
     * @return int the current index
     */
    public int getIndexCurrent(){ return model_move_number.getIndexCurrent(); }


    /**
     * Moves the character if he can and the block ahead (if there is one)
     * and increments the number of push if there is a block moves
     *
     * @param d the direction where the player wants to go
     * @return 1 if the move has succeed with a block
     * 0 if the move has succeed without a block
     * -1 if the move aren't possible
     */
    public int move(Direction d) {
        int res = model_move_number.move(d);
        if (res == 1) {
            push_number++;
        }
        return res;
    }

    /**
     * Undo the move made if there is one to undo,
     * decrements the number of movements if the moves undone
     * was a move with a block
     */
    public void undo() {
        int res = model_move_number.undo();
        if (res == 1) {
            push_number--;
        }
    }

    /**
     * redone the move undone earlier,
     * increments the number of movements if the
     * moves redone was a move with a block
     */
    public void redo() {
        int res = model_move_number.redo();
        if (res == 1) {
            push_number++;
        }
    }

    /**
     * Resets the game at the initial state
     */
    public void reset() {
        model_move_number.reset();
        push_number = 0;
    }


    /**
     * Temporary method
     */
    public void printGame() {
        model_move_number.printGame();
        System.out.println("Nombre de poussée : " + this.push_number);
    }
}

