package Model;


import loader.Coordinates;
import loader.Direction;

/**
 * A block of memory composed with the coordinates of the character
 * , the direction applied and a boolean which said if the block is
 * the current state
 */
public class memorySoko {

    /**
     * The position of the character
     */
    private Coordinates character;

    /**
     * The direction applied
     */
    private Direction d;

    /**
     * Is it the current state ?
     */
    private boolean isCurrentState;


    /**
     * Constructor with two parameters
     *
     * @param d         Direction the direction applied
     * @param character The position of the character
     */
    memorySoko(Direction d, Coordinates character) {
        this.d = d;
        this.character = character;
        this.isCurrentState = true;
    }

    /**
     * Constructor with two parameters
     *
     * @param d              Direction the direction applied
     * @param character      The position of the character
     * @param isCurrentState boolean which say if the element is the current state
     */
    memorySoko(Direction d, Coordinates character, boolean isCurrentState) {
        this.d = d;
        this.character = character;
        this.isCurrentState = isCurrentState;
    }

    /**
     * Constructor by copy
     *
     * @param m the memory we want to copy
     */
    public memorySoko(memorySoko m) {
        this.d = m.d;
        this.character = m.character;
        this.isCurrentState = m.isCurrentState;
    }

    /**
     * Gives a boolean which say if the element is a current state
     *
     * @return boolean true if the element is the current state else false
     */
    public boolean isCurrentState(){
        return isCurrentState;
    }

    /**
     * Gives the direction applied
     *
     * @return Direction the direction applied
     */
    public Direction getD() {
        return d;
    }

   /*
    public void setD(Direction d) {
        this.d = d;
    }*/

    /**
     * Gives the coordinates of the character
     *
     * @return Coordinates the coordinates of the character
     */
    public Coordinates getCharacter() {
        return character;
    }

    /*
    public void setCharacter(Coordinates character) {
        this.character = character;
    }*/

    /**
     * Changes the current state
     *
     * @param currentState boolean the new current state
     */
    void setCurrentState(boolean currentState) {
        isCurrentState = currentState;
    }

    /**
     * Create a string
     *
     * @return String a print of memorySoko
     */
    @Override
    public String toString() {
        return "memorySoko{" +
                "character=" + character +
                ", d=" + d +
                ", isCurrentState=" + isCurrentState +
                '}';
    }
}
