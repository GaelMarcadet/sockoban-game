package Model;

import loader.Coordinates;
import loader.Direction;
import loader.Game;
import loader.Type;

/**
 * Model's implementation
 */
public class ModelConcret implements Model {

    /**
     * The game asked by the user
     */
    private Game game;


    /**
     * Constructor without parameter
     */
    ModelConcret() {
        this.game = null;
    }

    /**
     * Constructor with one parameter
     *
     * @param game the game asked by the user
     */
    ModelConcret(Game game) {
        this.game = game;
    }

    /**
     * Gives the game
     *
     * @return Game the new game
     */
    public Game getGame() {
        return game;
    }

    /**
     * changes the game
     *
     * @param game the game which has replaced the old game
     */
    public void setGame(Game game) {
        this.game = game;
    }

    /**
     * Switches places two blocks
     *
     * @param type         the type that you wants to move
     * @param type_nextPos the type in the block where you wants to move
     * @param nextPos      the coordinates of the next position
     * @param pos          the coordinates of the current position
     */
    void switchPlace(Type type, Type type_nextPos, Coordinates nextPos, Coordinates pos) {
        Type[][] world = game.getMap();

        if (type == Type.PLAYER_ON_GOAL || type == Type.BOX_ON_GOAL) {
            //the block where the character or the box leaves is a goal
            world[pos.getX()][pos.getY()] = Type.EMPTY_GOAL;
        } else {
            //the block where the character or the box leaves is empty
            world[pos.getX()][pos.getY()] = Type.EMPTY;
        }

        if (type_nextPos == Type.EMPTY) {
            //the block where the character or the box moves is a empty
            switch (type) {
                case PLAYER_ON_FLOOR:
                    world[nextPos.getX()][nextPos.getY()] = type;
                    game.getCharacter().set(nextPos.getX(), nextPos.getY());
                    break;

                case PLAYER_ON_GOAL:
                    world[nextPos.getX()][nextPos.getY()] = Type.PLAYER_ON_FLOOR;
                    game.getCharacter().set(nextPos.getX(), nextPos.getY());
                    break;

                case BOX_ON_FLOOR:
                    world[nextPos.getX()][nextPos.getY()] = type;
                    break;

                case BOX_ON_GOAL:
                    world[nextPos.getX()][nextPos.getY()] = Type.BOX_ON_FLOOR;
                    game.setBoxOnGoal(game.getBoxsOnGoal()-1);
                    break;
            }
        } else {
            //the block where the character or the box moves is a goal
            switch (type) {
                case PLAYER_ON_FLOOR:
                    world[nextPos.getX()][nextPos.getY()] = Type.PLAYER_ON_GOAL;
                    game.getCharacter().set(nextPos.getX(), nextPos.getY());
                    break;

                case PLAYER_ON_GOAL:
                    world[nextPos.getX()][nextPos.getY()] = type;
                    game.getCharacter().set(nextPos.getX(), nextPos.getY());
                    break;

                case BOX_ON_FLOOR:
                    world[nextPos.getX()][nextPos.getY()] = Type.BOX_ON_GOAL;
                    game.setBoxOnGoal(game.getBoxsOnGoal()+1);
                    break;

                case BOX_ON_GOAL:
                    world[nextPos.getX()][nextPos.getY()] = type;
                    break;
            }
        }
    }


    /**
     * Moves the character if he can and the block ahead (if there is one)
     *
     * @param d the direction where the player wants to go
     * @return 1 if the move has succeed with a block
     * 0 if the move has succeed without a block
     * -1 if the move aren't possible
     */
    @Override
    public int move(Direction d) {
        int next_posX = game.getCharacter().getX() + d.getOffset_x();
        int next_posY = game.getCharacter().getY() + d.getOffset_y();
        Type[][] world = game.getMap();
        Type character = world[game.getCharacter().getX()][game.getCharacter().getY()];

        return moveBoxAndCharacter(d, character, new Coordinates(next_posX, next_posY));
    }

    /**
     * Main sub-program which differentiates each case of type and
     * call an other sub-program for switch place
     *
     * @param d           the direction where the player wants to go
     * @param t           the type that you wants to move
     * @param coordinates the coordinates of the next block
     * @return 1 if the move has succeed with a block
     * 0 if the move has succeed without a block
     * -1 if the move aren't possible
     */
    private int moveBoxAndCharacter(Direction d, Type t, Coordinates coordinates) {
        Type[][] world = game.getMap();

        // Verify if the square in front exist in the grid
        if (game.exists(coordinates.getX(), coordinates.getY())) {

            switch (world[coordinates.getX()][coordinates.getY()]) {
                case EMPTY:
                    //System.out.println("La case est vide");
                    //The square is empty
                    switchPlace(t, Type.EMPTY, coordinates, new Coordinates(coordinates.getX() - d.getOffset_x(), coordinates.getY() - d.getOffset_y()));
                    return 0;

                case EMPTY_GOAL:
                    //System.out.println("La case est vide sur un point d'intérêt");
                    //The square is empty with a goal
                    switchPlace(t, Type.EMPTY_GOAL, coordinates, new Coordinates(coordinates.getX() - d.getOffset_x(), coordinates.getY() - d.getOffset_y()));
                    return 0;

                case WALL:
                    //System.out.println("La case est un mur");
                    //The square is a wall
                    return -1;

                case BOX_ON_FLOOR:
                    //System.out.println("La case est une boîte");
                    //The square is a box
                    if (t == Type.BOX_ON_FLOOR || t == Type.BOX_ON_GOAL) {
                        return -1;
                    } else {
                        switch (moveBoxAndCharacter(d, world[coordinates.getX()][coordinates.getY()], new Coordinates(coordinates.getX() + d.getOffset_x(), coordinates.getY() + d.getOffset_y()))) {
                            case 0:
                                switchPlace(t, Type.EMPTY, coordinates, new Coordinates(coordinates.getX() - d.getOffset_x(), coordinates.getY() - d.getOffset_y()));
                                return 1;
                            case -1:
                                return -1;
                        }
                    }

                case BOX_ON_GOAL:
                    //System.out.println("La case est une boîte sur un point d'intérêt");
                    //The square is a box on a goal
                    if (t == Type.BOX_ON_FLOOR || t == Type.BOX_ON_GOAL) {
                        return -1;
                    } else {
                        switch (moveBoxAndCharacter(d, world[coordinates.getX()][coordinates.getY()], new Coordinates(coordinates.getX() + d.getOffset_x(), coordinates.getY() + d.getOffset_y()))) {
                            case 0:
                                switchPlace(t, Type.EMPTY_GOAL, coordinates, new Coordinates(coordinates.getX() - d.getOffset_x(), coordinates.getY() - d.getOffset_y()));
                                return 1;
                            case -1:
                                return -1;
                        }
                    }

                default:
                    System.out.println("Je n'ai pas trouvé de cas viable");
                    return -1;
            }
        } else {
            return -1;
        }
    }

    /**
     * Temporary method
     */
    void printGame() {
        Type[][] world = game.getMap();

        for (int i = 0; i < game.getHeight(); i++) {
            for (int j = 0; j < game.getWidth(); j++) {
                System.out.print(world[j][i]);
            }
            System.out.println();
        }
        System.out.println("Nombre de boîte : " + game.getBoxs());
        System.out.println("Nombre de boîte sur un objectif : " + game.getBoxsOnGoal());
    }
}

