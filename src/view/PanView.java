package view;

import controller.Controller;

import exceptions.NoPreviousDirectionsException;

import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

import loader.Direction;
import loader.Game;
import loader.Type;

import observer.Observer;

import java.util.function.Function;

/**
 * The PanView class generate the game-board display in the game window.
 * It implements the Observer interface.
 */
public class PanView implements Observer {

    /**
     * the Gridpane which serves as support for the game-board.
     */
    private GridPane grid;

    /**
     * Image used to see the character who goes up.
     */
    private static Image back = new Image(PanView.class.getResourceAsStream("/pictures/Character7.png"));
    /**
     * Image used to see the character who turns left.
     */
    private static Image left = new Image(PanView.class.getResourceAsStream("/pictures/Character1.png"));
    /**
     * Image used to see the character who turns right.
     */
    private static Image right = new Image(PanView.class.getResourceAsStream("/pictures/Character2.png"));
    /**
     * Image used to see the character who goes down.
     */
    private static Image front = new Image(PanView.class.getResourceAsStream("/pictures/Character4.png"));
    /**
     * Image used for the ground.
     */
    private static Image ground = new Image(PanView.class.getResourceAsStream("/pictures/GroundGravel_Grass.png"));
    /**
     * Image used for the walls.
     */
    private static Image wall = new Image(PanView.class.getResourceAsStream("/pictures/Wall_Brown.png"));
    /**
     * Image used for the boxes target.
     */
    private static Image target = new Image(PanView.class.getResourceAsStream("/pictures/EndPoint_Blue.png"));
    /**
     * Image used for the misplaced boxes.
     */
    private static Image box = new Image(PanView.class.getResourceAsStream("/pictures/CrateDark_Blue.png"));
    /**
     * Image used for the boxes on a target.
     */
    private static Image completed = new Image(PanView.class.getResourceAsStream("/pictures/Crate_Blue.png"));


    /**
     * Getteur of the Gridpane.
     * @return the Gridpane.
     */
    public GridPane getGridPane(){
        return grid;
    }

    /**
     * Default constructor, without parameter.
     */
    public PanView() {
        this.grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setPrefSize(Integer.MAX_VALUE, Integer.MAX_VALUE);
        grid.setGridLinesVisible(false);
        grid.setMinSize(0, 0);
    }


    /**
     * Redefine the implemented update method.
     * Change each images depend on the new statement of the game.
     */
    @Override
    public void update() {

        //Get the actual statement of the game
        Game g = Controller.getController().getFacadeModel().getGame();

        //Check if there is a game
        if (g != null) {

            double width = g.getWidth(), height = g.getHeight();
            double aux;


            if (grid.getHeight() < grid.getWidth())
                aux = grid.getHeight() / Double.max(width, height);
            else
                aux = grid.getWidth() / Double.max(width, height);

            //Resize the image based on the Gridane's node size
            Function<Image, ImageView> sizer = (image) -> {
                ImageView view = new ImageView(image);
                view.setFitWidth(aux);
                view.setFitHeight(aux);
                return view;
            };


            int column = 0;
            int row = 0;
            //reset grid
            grid.getChildren().remove(0, grid.getChildren().size());
            ImageView element;

            for (Game.Line line : g) {
                for (Type type : line) { //for each node
                    switch (type) {
                        case WALL:
                            grid.add(sizer.apply(wall), column, row);
                            break;
                        case EMPTY:
                            grid.add(sizer.apply(ground), column, row);
                            break;
                        case EMPTY_GOAL:
                            grid.add(sizer.apply(ground), column, row);
                            element = sizer.apply(target);
                            grid.add(element, column, row);
                            GridPane.setHalignment(element, HPos.CENTER);
                            break;
                        case BOX_ON_GOAL:
                            grid.add(sizer.apply(ground), column, row);
                            grid.add(sizer.apply(completed), column, row);
                            break;
                        case BOX_ON_FLOOR:
                            grid.add(sizer.apply(ground), column, row);
                            grid.add(sizer.apply(box), column, row);
                            break;
                        case PLAYER_ON_FLOOR:
                        case PLAYER_ON_GOAL:
                            Direction orientation;
                            try {
                                orientation = Controller.getController().lastPositionCommand().exec();
                            } catch (NoPreviousDirectionsException e) {
                                orientation = Direction.SOUTH;
                            }
                            grid.add(sizer.apply(ground), column, row);
                            switch (orientation) {//Determine on which direction the character moves on
                                case WEST:
                                    element = sizer.apply(left);
                                    break;
                                case NORTH:
                                    element = sizer.apply(back);
                                    break;
                                case EAST:
                                    element = sizer.apply(right);
                                    break;
                                case SOUTH:
                                default:
                                    element = sizer.apply(front);
                                    break;
                            }
                            grid.add(element, column, row);
                            GridPane.setHalignment(element,HPos.CENTER);
                    }
                    column++;
                }
                column = 0;
                ++row;
            }
            grid.setGridLinesVisible(false);
        }
    }

}