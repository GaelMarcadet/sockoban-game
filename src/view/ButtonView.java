package view;


import javafx.scene.control.Button;
import observer.Observer;

/***
 * The ButtonView class generate the buttons used in the game window
 * to carry out four different actions.
 * It implements the Observer interface.
 */
public class ButtonView implements Observer {

    /**
     * Button that reset the game.
     */
    private Button reset;
    /**
     * Button that undo the previous move.
     */
    private Button undo;
    /**
     * Button that redo the following move.
     */
    private Button redo;
    /**
     * Button that animate that game, from the initial statement of the game, to the last move done.
     */
    private Button animated;

    /**
     * Create the "Reset" button.
     * @return Button named Reset.
     */
    private Button resetButton() {
        return new Button("Reset");
    }

    /**
     * Create the "Undo" button.
     * @return Button named Undo.
     */
    private Button undoButton() {
        return new Button("Undo");
    }

    /**
     * Create the "Redo" button.
     * @return Button named Redo.
     */
    private Button redoButton() {
        return new Button("Redo");
    }

    /**
     * Create the "Animated" button.
     * @return Button named Animated.
     */
    private Button animatedButton() {
        return new Button("Animate");
    }

    /***
     *The default constructor, without parameters
     */
    public ButtonView() {
        this.reset = resetButton();
        this.undo = undoButton();
        this.redo = redoButton();
        this.animated = animatedButton();

    }

    /**
     * Getter of the Reset Button.
     * @return The Reset Button.
     */
    public Button getReset() {
        return reset;
    }

    /**
     * Getter of the Undo Button.
     * @return The Undo Button.
     */
    public Button getUndo() {
        return undo;
    }

    /**
     * Getter of the Redo Button.
     * @return The Redo Button.
     */
    public Button getRedo() {
        return redo;
    }

    /**
     * Getter of the Animated Button.
     * @return The Animated Button.
     */
    public Button getAnimated() {
        return animated;
    }

    /**
     * The update method has no actions for ButtonView
     */
    @Override
    public void update() {

    }
}
