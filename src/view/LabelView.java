package view;

import commands.IntCommand;
import controller.Controller;
import javafx.scene.control.Label;
import observer.Observer;

/**
 * The LabelView class generate the labels used in the game window
 * to show two different news.
 * It implements the Observer interface.
 */
public class LabelView implements Observer {

    /**
     * The label that shows the number of valid movements of the player (character).
     */
    private Label nbrAction;
    /**
     * The label that shows the number of boxes movements
     */
    private Label nbrPush;

    /**
     * The {@code IntCommand} used to get information about the different movements
     */
    private IntCommand pushNumber, moveNumber;

    /**
     * Create the Move label.
     * @return The Label about movements.
     */
    private Label nbrActionLabel(){
        return new Label("Move number : 0");
    }

    /**
     * Create the Push label
     * @return The Label about box pushes
     */
    private Label nbrPushLabel(){
        return new Label("Push number : 0");
    }

    /**
     * The default constructor, without parameter.
     */
    public LabelView(){
        this.nbrAction= nbrActionLabel();
        this.nbrPush= nbrPushLabel();

        Controller controller = Controller.getController();
        pushNumber = controller.getPushNbr();
        moveNumber = controller.getMoveNbr();
    }

    /**
     * Getteur of the Move Label.
     * @return the Label about movements.
     */
    public Label getNbrAction() {
        return nbrAction;
    }

    /**
     * Getteur of the Push Label.
     * @return the Label about box pushes.
     */
    public Label getNbrPush() {
        return nbrPush;
    }

    /**
     * Redefine the implemented update method.
     * Changes the number of moves and pushes in the labels.
     */
    @Override
    public void update() {
        nbrAction.setText("Move number : " + moveNumber.exec());
        nbrPush.setText("Push number : " + pushNumber.exec());
    }
}
