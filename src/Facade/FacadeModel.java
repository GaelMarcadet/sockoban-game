package Facade;

import Model.*;
import loader.Direction;
import loader.Game;

import java.util.ArrayList;

/**
 * Class which be used as a Facade for an utilisation
 * more easily of the model
 */
public class FacadeModel {

    /**
     * An instance of ModelPushNumber for
     * instantiating a game
     */
    private ModelPushNumber model_push_number;


    /**
     * Constructor without parameter
     */
    public FacadeModel() {
        this.model_push_number = new ModelPushNumber();
    }

    /**
     * Constructor with one parameter
     *
     * @param game The game asked by the user
     */
    public FacadeModel(Game game) {
        this.model_push_number = new ModelPushNumber(game);
    }


    /**
     * Changes the current game
     *
     * @param game the new game asked by the user
     */
    public void setGame(Game game) {
        this.model_push_number.setGame(game);
    }

    /**
     * Gives the current game
     *
     * @return game the current game
     */
    public Game getGame() {
        return model_push_number.getGame();
    }

    /**
     * Gives move's number
     *
     * @return int the move's number
     */
    public int getMove_number() {
        return model_push_number.getMove_number();
    }

    /**
     * Gives move's number of undone
     *
     * @return int the move's number of undone
     */
    public int getMove_number_undo() {
        return model_push_number.getMove_number_undo();
    }

    /**
     * Gives push's number
     *
     * @return int the push's number
     */
    public int getPush_number() {
        return model_push_number.getPush_number();
    }

    /**
     * Gives a copy of the memory
     *
     * @return ArrayList a copy of the memory
     */
    public ArrayList<memorySoko> getMemory() {
        ArrayList<memorySoko> clone = new ArrayList<memorySoko>();
        if(this.model_push_number.getIndexCurrent() > -1) {
            for (memorySoko m : model_push_number.getMemory()) {
                if (m instanceof memoryBox) {
                    clone.add(new memoryBox((memoryBox) m));
                } else {
                    clone.add(new memorySoko(m));
                }
                if(m.isCurrentState()){ break; }
            }
        }
        return clone;
    }

    /**
     * Moves the character if he can and the block ahead (if there is one)
     *
     * @param d the direction where the player wants to go
     * @return 1 if the move has succeed with a block
     * 0 if the move has succeed without a block
     * -1 if the move aren't possible
     */
    public int move(Direction d) {

        return model_push_number.move(d);
    }

    /**
     * Undo the move made if there is one to undo
     */
    public void undo() {
        model_push_number.undo();
    }

    /**
     * redone the move undone earlier
     */
    public void redo() {
        model_push_number.redo();
    }

    /**
     * Resets the game at the initial state
     */
    public void reset() {
        model_push_number.reset();
    }


    /**
     * Temporary method
     */
    public void printGame() {
        model_push_number.printGame();
    }
}
