package controller;

import javafx.scene.Scene;
import javafx.scene.input.KeyCode;

import loader.Direction;
import view.ButtonView;
import view.PanView;

/**
 * The class {@code ControllerDX} is used to bind actions to keyboard and to buttons.
 */
public class ControllerFX {

    /**
     * The controller where methods to modify the model are stored
     */
    private Controller controller;

    /**
     * A part of the view
     */
    private PanView panview;

    /**
     * A part of the view that store all the buttons and labels
     */
    private ButtonView buttonView;

    /**
     * The scene of the JavaFX application
     */
    private Scene scene;

    /**
     * Builder of the ControllerFx class
     *
     * @param controller Controller where all methods triggering the model are to be found
     * @param panview
     * @param buttonView
     */
    public ControllerFX(Controller controller, PanView panview, ButtonView buttonView, Scene scene) {


        this.controller = controller;
        this.panview = panview;
        this.buttonView = buttonView;
        this.scene = scene;

        setActions();

    }

    /**
     * Bind actions to keys of the user keyboard and to the button of the GUI
     * */
    public void setActions() {
        //For the 4 directionals keys and the four qzsd keys of the keyboard, Soko moving methos is triggered
        scene.setOnKeyPressed(event -> {
            KeyCode code = event.getCode();
            if (KeyCode.DOWN == code || KeyCode.S == code)
                controller.move(Direction.SOUTH);
            else if (KeyCode.UP == code || KeyCode.Z == code)
                controller.move(Direction.NORTH);

            else if (KeyCode.LEFT == code || KeyCode.D == code)
                controller.move(Direction.WEST);

            else if (KeyCode.RIGHT == code || KeyCode.Q == code)
                controller.move(Direction.EAST);

        });
        //Bind actions to each buttons
        buttonView.getReset().setOnAction(e -> controller.reset());
        buttonView.getUndo().setOnAction(e -> controller.undo());
        buttonView.getRedo().setOnAction(e -> controller.redo());
        buttonView.getAnimated().setOnAction(e -> controller.animate());
    }

    /**
     * Bind no actions to keys of the user keyboard and to the button of the GUI, it delete every other actions that were bound before
     * */
    public void noActions() {

        //For the 4 directionals keys and the four qzsd keys of the keyboard, all actions are deleted
        scene.setOnKeyPressed(e -> {});
        //Delete all actions for each buttons
        buttonView.getReset().setOnAction(e -> {});
        buttonView.getUndo().setOnAction(e -> {});
        buttonView.getRedo().setOnAction(e -> {});
        buttonView.getAnimated().setOnAction(e -> {});
    }
}





