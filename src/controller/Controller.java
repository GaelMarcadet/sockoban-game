package controller;

import Facade.FacadeModel;
import Model.memorySoko;
import commands.GameCommand;
import commands.IntCommand;
import commands.LastDirectionCommand;
import exceptions.NoPreviousDirectionsException;
import javafx.application.Platform;

import javafx.scene.control.Alert;
import loader.Direction;
import loader.Game;
import observer.Observable;
import observer.Observer;

import java.util.ArrayList;


/**
 * The class Controller {@code Controller} is used to implement the controller pattern.
 * It compute user actions, trigger model methods and modify the view if model state changes
 * It also implements the singleton pattern, there can be only one instance of this class
 */

public class Controller implements Observable {


    /**
     * Controller this
     */
    private static Controller singleton;

    /**
     * FacadeModel allow the controller to access methods that will trigger actions on model without accessing the inner model directly
     */
    private FacadeModel facadeModel;

    /**
     * List of the observers of the controller, the observers might be the view elements
     */
    private ArrayList<Observer> observers = new ArrayList<>();


    /**
     * Allow other classes to access or create the controller without creating a new one if it has already been created
     * replacing the builder of the class that is private     *
     *
     * @return the Controller this
     */


    private ControllerFX controllerFX;


    /**
     * Return a static instanc eoa the Controller if it already exist and create another otherwise
     *
     * @return the controller This
     */
    public static Controller getController() {
        if (singleton == null)
            singleton = new Controller();
        return singleton;

    }

    /**
     * Getter of the FacadModel of the class
     *
     * @return the FacadeModel of the class
     */
    public FacadeModel getFacadeModel() {

        return this.facadeModel;
    }


    /**
     * Setter of the attribute ControllerFx of the class
     *
     * @param controllerFX the new ControllerFX
     */
    public void setControllerFX(ControllerFX controllerFX) {


        this.controllerFX = controllerFX;

    }


    /**
     * Builder of the class
     * declared private so it can be used in an extern class, creating multiple instance of the singleton
     */
    private Controller() {
        this.facadeModel = new FacadeModel();
    }

    /**
     * Change the game in the model
     */
    public void setGame(Game game) {

        facadeModel.setGame(game);
        notifie();
        this.controllerFX.setActions();


    }

    /**
     * Add an Observer to the list of Observers
     *
     * @param observer The Observer to be add
     */
    @Override
    public void addObserver(Observer observer) {
        this.observers.add(observer);
    }


    /**
     * Notify all the observers that are contained in the list observers
     */
    @Override
    public void notifie() {
        for (Observer n : observers) {
            n.update();
        }
    }

    /**
     * Move Soko in the direction dir.
     *
     * @param dir Direction Soko is heading
     */
    public void move(Direction dir) {
        this.facadeModel.move(dir);
        notifie();
        // If the user have won, it trigger a pop-up to inform him of his victory and all moving, reseting etc...
        // actions are disabled until the user choose another map to play on
        if (hasWon()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);

            alert.setTitle("CONGRATULATIONS YOU WON THIS ROUND");

            int random =(int) (Math.random() * (5));
            switch (random){
                case 0:
                    alert.setHeaderText("You are such a good player");
                    break;
                case 1:
                    alert.setHeaderText("Hats off to you");
                    break;
                case 2:
                    alert.setHeaderText("Congrats");
                    break;
                case 3:
                    alert.setHeaderText("Keep up the good work");
                    break;
                default:
                    alert.setHeaderText("That was good, you should consider professionnal Sokoban gaming");
                    break;

            }

            alert.setContentText("You must select a game in the left panel to load a new game.");

            alert.showAndWait();

            this.controllerFX.noActions();
        }

    }


    /**
     * Reset the game.
     */
    public void reset() {

        this.facadeModel.reset();
        notifie();

    }

    /**
     * Undo the last action of the game
     */
    public void undo() {
        this.facadeModel.undo();
        notifie();
    }

    /**
     * Redo the last action of the game.
     */
    public void redo() {
        this.facadeModel.redo();
        notifie();
    }

    /**
     * Play all states of the game from the beginning to the end.
     * There is a sleep between each state of the game so that the player can see what is happening
     */

    public void animate() {
        ArrayList<memorySoko> list = facadeModel.getMemory();
        this.facadeModel.reset();
        notifie();
        //All actions are disabled during the animation to prevent the user from creating an error in the model
        this.controllerFX.noActions();
        Thread lol = new Thread(() -> {
            for (memorySoko m : list) {

                try {
                    Thread.sleep(400);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


                Platform.runLater(() -> this.facadeModel.move(m.getD()));

                Platform.runLater(() -> notifie());


            }
            this.controllerFX.setActions();

        });
        lol.start();

    }

    /**
     * Check if the user have won the round
     *
     * @return true if the user have won false otherwise
     */
    public boolean hasWon() {

        Game g = this.facadeModel.getGame();
        return g.getBoxsOnGoal() == g.getBoxs();


    }


    /**
     * Command to retrieve the last direction Soko as been walking, can be used to know which direction
     * Soko is currently facing
     */
    public LastDirectionCommand lastPositionCommand() {
        return () -> {
            ArrayList<memorySoko> memory = facadeModel.getMemory();
            if (facadeModel.getMemory().size() == 0)
                throw new NoPreviousDirectionsException("There is no previous directions, the game just started");
            return memory.get((facadeModel.getMemory().size()) - 1).getD();
        };


    }

    /**
     * Command to retrieve the number of push of Soko on the boxes since the beginning of the round
     */
    public IntCommand getPushNbr() {
        return () -> facadeModel.getPush_number();
    }

    /**
     * Command to retrieve the number of move of Soko since the beginning of the round
     */
    public IntCommand getMoveNbr() {
        return () -> facadeModel.getMove_number();
    }

    /**
     * Command to retrieve a Game from the model
     */
    public GameCommand getGame() {

        return () -> facadeModel.getGame();
    }

}
