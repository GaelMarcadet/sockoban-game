package commands;

import loader.Game;
import loader.Type;


/**
 * The interface GameCommand {@code GameCommand} is used to implement the command pattern.
 * Its goal is to return a game
 */
public interface GameCommand {

    /**
     * Must be implemented to return a Game
     */
    public Game exec();
}
